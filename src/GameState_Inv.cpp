#include "GameState_Inv.h"
#include "GameState_Play.h"
#include "Common.h"
#include "Assets.h"
#include "GameEngine.h"
#include "Components.h"

GameState_Inv::GameState_Inv(GameEngine & game)
	: GameState(game)
{
	init("");
}

sf::Sprite background1;
sf::Sprite background2;
sf::Sprite background3;
sf::Texture backgroundTex1;
sf::Texture backgroundTex2;
sf::Texture backgroundTex3;
void GameState_Inv::init(const std::string & levelPath)
{
	if (!backgroundTex1.loadFromFile("images/BorderTemplate.png"))
	{
		std::cout << "Error loading background";
	}
	if (!backgroundTex2.loadFromFile("images/Weapon_70.png"))
	{
		std::cout << "Error loading background";
	}
	if (!backgroundTex3.loadFromFile("images/Weapon_61.png"))
	{
		std::cout << "Error loading background";
	}
	background1.setTexture(backgroundTex1);
	background2.setTexture(backgroundTex2);
	background3.setTexture(backgroundTex3);
	
	background2.setScale(10.f, 10.f);
	background3.setScale(10.f, 10.f);
	background1.setScale(10.f, 10.f);

	m_menuText.setFont(m_game.getAssets().getFont("Mana"));
	m_menuText.setCharacterSize(64);
	m_menuText.setString("Inventory");
}

void GameState_Inv::update()
{
	//Play music when state is in focus
	
	m_entityManager.update();

	sUserInput();
	sRender();
	
}

bool startTransition1 = false;
void GameState_Inv::sUserInput()
{
	sf::Event event;
	while (m_game.window().pollEvent(event))
	{
		// this event is triggered when a key is pressed
		if (event.type == sf::Event::KeyPressed)
		{
			switch (event.key.code)
			{
				case sf::Keyboard::Tab:
				case sf::Keyboard::Escape:
				{
					m_game.popState();
					break;
				}
			}
		}
	}

}

void GameState_Inv::sRender()
{
	background2.setPosition(sf::Vector2f(350, 150));
	background3.setPosition(sf::Vector2f(600, 150));
	background1.setPosition(sf::Vector2f(275, 13));
	m_menuText.setPosition(sf::Vector2f(330, 50));
	m_game.window().draw(background2);
	m_game.window().draw(background3);
	m_game.window().draw(background1);
	m_game.window().draw(m_menuText);
	
	m_game.window().display();
}

