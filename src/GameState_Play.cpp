#include "GameState_Play.h"
#include "GameState_Inv.h"
#include "Common.h"
#include "Physics.h"
#include "Assets.h"
#include "GameEngine.h"
#include "Components.h"
#include <cmath>

static const char SHADER_LIGHT_VERT[] =
"void main()"
"{"
"gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;"
"gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;"
"gl_FrontColor = gl_Color;"
"}";

static const char SHADER_LIGHT_FRAG[] =
"uniform vec3 light;"
"uniform float a;"
"void main(void)"
"{"
"float distance = sqrt(pow(gl_FragCoord.x - light.x, 2) + pow(gl_FragCoord.y - light.y, 2));"
"float alpha = 1.;"

"if (distance <= light.z)"
"{"
"alpha = (a / light.z) * distance;"
"}"
"gl_FragColor = vec4(0., 0., 0., alpha);"

"}";


bool m_displayShader = true;
bool m_viewChange = false;
bool m_levelCompleted;
std::string weapons;
int light = 9;
GameState_Play::GameState_Play(GameEngine & game, const std::string & levelPath, const std::string & levelName)
	: GameState(game)
	, m_levelPath(levelPath), m_levelName(levelName)
{
	m_levelCompleted = false;
    init(m_levelPath);
}

void GameState_Play::init(const std::string & levelPath)
{
    loadLevel(levelPath);

	//Change music depending on level
	m_bg_music = m_entityManager.addEntity("music");
	if (levelPath == "level4.txt")
	{
		m_bg_music->addComponent<CMusic>(m_game.getAssets().getMusic("PlayBoss"), true, true);
	}
	else
	{
		m_bg_music->addComponent<CMusic>(m_game.getAssets().getMusic("PlayBG"), true, true);
	}

	int windowX = m_game.window().getSize().x;
	int windowY = m_game.window().getSize().y;

	//Fade in transition
	m_fadeIn = m_entityManager.addEntity("fadeIn");
	m_fadeIn->addComponent<CText>(m_game.getAssets().getFont("Mana"), m_levelName, Vec2(windowX / 2.0f, windowY - (windowY * 0.3f)), 52, sf::Color::Red);
	m_fadeIn->addComponent<CLifeSpan>(3000);
	m_fadeIn->addComponent<CRectangleShape>(Vec2(windowX, windowY), Vec2(0, 0), sf::Color(0, 0, 0, 255));
}

void GameState_Play::loadLevel(const std::string & filename)
{
    m_entityManager = EntityManager();

	//Read in level config file
	std::ifstream fin(m_levelPath);
	std::string token;

	while (fin.good())
	{
		fin >> token;
		
		if (token == "Player")
		{
			int posX, posY;
			int bX, bY;
			float player_speed;
			int HP;
			int arm;

			fin >> posX >> posY >> bX >> bY >> player_speed >> HP >> arm;

			m_playerConfig.X = posX;
			m_playerConfig.Y = posY;
			m_playerConfig.CX = bX;
			m_playerConfig.CY = bY;
			m_playerConfig.SPEED = player_speed;
			m_playerConfig.HitPoints = HP;
			m_playerConfig.Armor = arm;
		}
		else if (token == "Tile")
		{
			std::string name;
			int rX, rY;
			int posX, posY;
			int bM, bV;

			fin >> name >> rX >> rY >> posX >> posY >> bM >> bV;

			// Calculate where to add tile entities
			Vec2 tilePos = Vec2(rX*1280, rY*768) + Vec2(posX*64 + 32, posY*64 + 32);

			auto tiles = m_entityManager.addEntity("tile");

			tiles->addComponent<CTransform>(tilePos);
			tiles->addComponent<CAnimation>(m_game.getAssets().getAnimation(name), true);
			tiles->addComponent<CBoundingBox>(m_game.getAssets().getAnimation(name).getSize(), bM, bV);
		
			if (name == "GravityWell")
			{
				tiles->addComponent<CGravity>(4.f, 200.f, true);
				tiles->addComponent<CLifeSpan>(5000);
			}
					}
		else if (token == "Item")
		{
			std::string name;
			int RX, RY, TX, TY, BM, BV, W, P, L;
			fin >> name >> RX >> RY >> TX >> TY >> BM >> BV >> W >> P >> L;
			auto item = m_entityManager.addEntity(token);
			item->addComponent<CTransform>((Vec2(RX * 1280, RY * 768) + Vec2(TX * 64 + 32, TY * 64 + 32)));
			item->addComponent<CAnimation>(m_game.getAssets().getAnimation(name), true);
			if (name == "BowRight")
			{
				item->getComponent<CTransform>()->scale.x = 1;
				item->getComponent<CTransform>()->scale.y = 1;
			}
			item->addComponent<CBoundingBox>(m_game.getAssets().getAnimation(name).getSize(), BM, BV);
			item->addComponent<CItem>(W, P, L);
		}
		else if (token == "NPC")
		{
			std::string name;
			int rX, rY;
			int posX, posY;
			int bM, bV;
			std::string AI_type;
			int Speed;
			int hp;

			fin >> name >> rX >> rY >> posX >> posY >> bM >> bV >> hp >> AI_type >> Speed;

			// Calculate where to add NPCs
			Vec2 NPC_Pos = Vec2(rX * 1280, rY * 768) + Vec2(posX * 64 + 32, posY * 64 + 32);

			auto enemies = m_entityManager.addEntity("npc");

			enemies->addComponent<CTransform>(NPC_Pos);
			enemies->addComponent<CAnimation>(m_game.getAssets().getAnimation(name), true);
			enemies->addComponent<CBoundingBox>(m_game.getAssets().getAnimation(name).getSize(), bM, bV);
			enemies->addComponent<Health>(hp);
			
			//AI behavour
			if (AI_type == "Follow")
			{
				enemies->addComponent<CFollowPlayer>(NPC_Pos, Speed);
			}
			else if(AI_type == "Patrol")
			{
				int numPatrolPoints;
				int X, Y;
				
				fin >> numPatrolPoints;

				//Coords for each patrol point
				std::vector<Vec2> patrolPoints;

				//Add each patrol point to the correct location 
				for (int i = 0; i < numPatrolPoints; i++)
				{
					int x, y;
					fin >> x >> y;
					patrolPoints.push_back(Vec2(rX * 1280, rY * 768) + Vec2(x * 64 + 32, y * 64 + 32));
				}

				enemies->addComponent<CPatrol>(patrolPoints, Speed);
			}
			if (name == "Monster4")
			{
				
				enemies->addComponent<Health>(100);
			}
		}
		else if (token == "Exit")
		{
			int rX, rY;
			int posX, posY;

			fin >> rX >> rY >> posX >> posY;

			// Calculate where to add tile entities
			Vec2 pos = Vec2(rX * 1280, rY * 768) + Vec2(posX * 64 + 32, posY * 64 + 32);

			m_exit = m_entityManager.addEntity("exit");
			m_exit->addComponent<CBoundingBox>(Vec2(64, 64), false, false);
			m_exit->addComponent<CTransform>(pos);
		}
	}

    //Spawn player
    spawnPlayer();
}

void GameState_Play::spawnPlayer()
{
    m_player = m_entityManager.addEntity("player");
    m_player->addComponent<CTransform>(Vec2(m_playerConfig.X, m_playerConfig.Y));
    m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandDown"), true);
    m_player->addComponent<CInput>();
	m_player->addComponent<CBoundingBox>(Vec2(m_playerConfig.CX, m_playerConfig.CY), true, true);
	m_player->addComponent<CState>("Standing");
	m_player->addComponent<Health>(10);
	m_player->addComponent<Armor>(50);
	m_player->addComponent<CInventory>();
	m_player->getComponent<CInventory>()->weapons.push_back("sword");
    m_player->getComponent<CTransform>()->facing = Vec2(0, 1);
}

void GameState_Play::spawnSword(std::shared_ptr<Entity> entity)
{
    auto eTransform = entity->getComponent<CTransform>();

    auto sword = m_entityManager.addEntity ("sword");
    sword->addComponent<CTransform>(entity->getComponent<CTransform>()->pos + eTransform->facing * 64);

	//Horizontal
	if (eTransform->facing.x != 0)
	{
		sword->addComponent<CAnimation>(m_game.getAssets().getAnimation(m_player->getComponent<CInventory>()->wepMapRight[m_player->getComponent<CInventory>()->weapons[m_player->getComponent<CInventory>()->sel]]), true);
		//sword->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordRight"), true);

		//Invert animation on X axis
		if (eTransform->facing.x == -1)
		{
			sword->getComponent<CTransform>()->scale.x *= -1;
		}
		if (m_player->getComponent<CInventory>()->wepMapRight[m_player->getComponent<CInventory>()->weapons[m_player->getComponent<CInventory>()->sel]] == "BowRight")
		{
			spawnArrow(sword);
		}
	}
	//Vertical
	else
	{
		sword->addComponent<CAnimation>(m_game.getAssets().getAnimation(m_player->getComponent<CInventory>()->wepMapUp[m_player->getComponent<CInventory>()->weapons[m_player->getComponent<CInventory>()->sel]]), true);
		//sword->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordUp"), true);

		//Invert animation on Y axis
		if (eTransform->facing.y == 1)
		{
			sword->getComponent<CTransform>()->scale.y *= -1;
		}
		if (m_player->getComponent<CInventory>()->wepMapUp[m_player->getComponent<CInventory>()->weapons[m_player->getComponent<CInventory>()->sel]] == "BowUp")
		{

			spawnArrow(sword);
		}
	}

	sword->addComponent<CBoundingBox>(sword->getComponent<CAnimation>()->animation.getSize(), false, false);
	sword->addComponent<CLifeSpan>(150);
	m_player->getComponent<CInventory>()->weapons.at(0) = sword->tag();
}

void GameState_Play::spawnArrow(std::shared_ptr<Entity> entity)
{
	auto eTransform = m_player->getComponent<CTransform>();
	auto arrow = m_entityManager.addEntity("arrow");
	std::cout << "Scale (X, Y): " << eTransform->scale.x << " , " << eTransform->scale.y << "\n";
	std::cout << "Facing (X, Y): " << eTransform->facing.x << " , " << eTransform->facing.y << "\n";


	if (m_player->getComponent<CTransform>()->facing.x == 0)
	{
		/*if (eTransform->facing.x == -1)
		{
		arrow->getComponent<CTransform>()->scale.x *= -1;

		}*/
		if (eTransform->facing.y == -1) // up
		{
			arrow->addComponent<CAnimation>(m_game.getAssets().getAnimation("ArrowUp"), true);
			arrow->addComponent<CTransform>(Vec2(m_player->getComponent<CTransform>()->pos.x, m_player->getComponent<CTransform>()->pos.y), Vec2(0, -10.0), Vec2(1, 1), 0);
		}
		else
		{

			arrow->addComponent<CAnimation>(m_game.getAssets().getAnimation("ArrowUp"), true);
			arrow->addComponent<CTransform>(Vec2(m_player->getComponent<CTransform>()->pos.x, m_player->getComponent<CTransform>()->pos.y), Vec2(0, 10.0), Vec2(1, 1), 180);
			std::cout << "here" << "\n";
		}
	}
	else
	{

		if (eTransform->facing.x == -1) // left
		{
			arrow->addComponent<CAnimation>(m_game.getAssets().getAnimation("ArrowRight"), true);
			arrow->addComponent<CTransform>(Vec2(m_player->getComponent<CTransform>()->pos.x, m_player->getComponent<CTransform>()->pos.y), Vec2(-10.0, 0), Vec2(-1, 1), 0);
		}
		else
		{
			arrow->addComponent<CAnimation>(m_game.getAssets().getAnimation("ArrowUp"), true);
			arrow->addComponent<CTransform>(Vec2(m_player->getComponent<CTransform>()->pos.x, m_player->getComponent<CTransform>()->pos.y), Vec2(10.0, 0), Vec2(1, 1), 0);
		}
	}
	arrow->addComponent<CBoundingBox>(entity->getComponent<CAnimation>()->animation.getSize(), false, false);
	arrow->addComponent<CLifeSpan>(500);
}

void GameState_Play::spawnBow(std::shared_ptr<Entity> entity)
{

}



const int SPECIAL_DURATION = 2000;
const int SPECIAL_SPEED = 10;
void GameState_Play::spawnSpecial(std::shared_ptr<Entity> entity)
{
	auto eTransform = entity->getComponent<CTransform>();

	auto special = m_entityManager.addEntity("special");
	special->addComponent<CTransform>(entity->getComponent<CTransform>()->pos + eTransform->facing * 64);
	

	//Horizontal
	if (eTransform->facing.x != 0)
	{
		special->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordRight"), true);
		special->getComponent<CTransform>()->speed = Vec2(SPECIAL_SPEED, 0);

		//Invert animation on X axis
		if (eTransform->facing.x == -1)
		{
			special->getComponent<CTransform>()->scale.x *= -1;
			special->getComponent<CTransform>()->speed = Vec2(-SPECIAL_SPEED, 0);
		}
	}
	//Vertical
	else
	{
		special->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordUp"), true);
		special->getComponent<CTransform>()->speed = Vec2(0, -SPECIAL_SPEED);

		//Invert animation on Y axis
		if (eTransform->facing.y == 1)
		{
			special->getComponent<CTransform>()->scale.y *= -1;
			special->getComponent<CTransform>()->speed = Vec2(0, SPECIAL_SPEED);
		}
	}

	special->addComponent<CBoundingBox>(special->getComponent<CAnimation>()->animation.getSize(), false, false);
	special->addComponent<CLifeSpan>(SPECIAL_DURATION);
}

void GameState_Play::toggleWeaponLeft()
{
	if (m_player->getComponent<CInventory>()->canToggle)
	{
		std::cout << m_player->getComponent<CInventory>()->weapons.size();
		if (m_player->getComponent<CInventory>()->sel - 1 >= 0)
		{
			m_player->getComponent<CInventory>()->sel--;
		}
		else
		{
			m_player->getComponent<CInventory>()->sel++;
		}
	}
}

void GameState_Play::toggleWeaponRight()
{
	if (m_player->getComponent<CInventory>()->canToggle)
	{
		if (m_player->getComponent<CInventory>()->sel + 1 < m_player->getComponent<CInventory>()->weapons.size())
		{
			m_player->getComponent<CInventory>()->sel++;
		}
		else
		{
			m_player->getComponent<CInventory>()->sel--;
		}
	}
}

void GameState_Play::update()
{
	//Pop back to main menu if level is completed
	if (m_levelCompleted)
	{
		m_game.popState();
	}

    m_entityManager.update();

    if (!m_paused)
    {
        sAI();
		sGravity();
		sCollision();
        sMovement();
        sLifespan();
        sAnimation();
    }

    sUserInput();
    sRender();
}

void GameState_Play::sMovement()
{
	//Reset speed every frame
	m_player->getComponent<CTransform>()->speed.x = 0;
	m_player->getComponent<CTransform>()->speed.y = 0;	

	if (m_player->hasComponent<CTransform>())
	{
		//Deny player movement if they died
		if (m_player->getComponent<CState>()->state == "Dead")
		{
			m_player->getComponent<CTransform>()->speed = Vec2(0.0f, 0.0f);
		}
		else
		{
			//Vertical movement
			if (m_player->getComponent<CInput>()->up && !m_player->getComponent<CInput>()->down)
			{
				m_player->getComponent<CTransform>()->speed.x = 0;
				m_player->getComponent<CState>()->state = "RunUp";
				m_player->getComponent<CTransform>()->facing = Vec2(0, -1);
				m_player->getComponent<CTransform>()->speed.y -= m_playerConfig.SPEED;
			}
			else if (m_player->getComponent<CInput>()->down && !m_player->getComponent<CInput>()->up)
			{
				m_player->getComponent<CTransform>()->speed.x = 0;
				m_player->getComponent<CState>()->state = "RunDown";
				m_player->getComponent<CTransform>()->facing = Vec2(0, 1);
				m_player->getComponent<CTransform>()->speed.y += m_playerConfig.SPEED;
			}

			//Horizontal movement
			if (m_player->getComponent<CInput>()->left && !m_player->getComponent<CInput>()->right)
			{
				m_player->getComponent<CTransform>()->speed.y = 0;
				m_player->getComponent<CState>()->state = "RunLeft";
				m_player->getComponent<CTransform>()->facing = Vec2(-1, 0);
				m_player->getComponent<CTransform>()->speed.x -= m_playerConfig.SPEED;
			}
			else if (m_player->getComponent<CInput>()->right && !m_player->getComponent<CInput>()->left)
			{
				m_player->getComponent<CTransform>()->speed.y = 0;
				m_player->getComponent<CState>()->state = "RunRight";
				m_player->getComponent<CTransform>()->facing = Vec2(1, 0);
				m_player->getComponent<CTransform>()->speed.x += m_playerConfig.SPEED;
			}

			//No movement (Standing)
			if (!m_player->getComponent<CInput>()->up && !m_player->getComponent<CInput>()->down && !m_player->getComponent<CInput>()->left && !m_player->getComponent<CInput>()->right)
			{
				//Vertical
				if (m_player->getComponent<CTransform>()->facing.x == 0 && m_player->getComponent<CTransform>()->facing.y == -1)
				{
					m_player->getComponent<CTransform>()->speed.x = 0;
					m_player->getComponent<CState>()->state = "StandUp";
				}
				else if (m_player->getComponent<CTransform>()->facing.x == 0 && m_player->getComponent<CTransform>()->facing.y == 1)
				{
					m_player->getComponent<CTransform>()->speed.x = 0;
					m_player->getComponent<CState>()->state = "StandDown";
				}
				//Horizontal
				else if (m_player->getComponent<CTransform>()->facing.y == 0 && m_player->getComponent<CTransform>()->facing.x == -1)
				{
					m_player->getComponent<CTransform>()->speed.y = 0;
					m_player->getComponent<CState>()->state = "StandLeft";
				}
				else if (m_player->getComponent<CTransform>()->facing.y == 0 && m_player->getComponent<CTransform>()->facing.x == 1)
				{
					m_player->getComponent<CTransform>()->speed.y = 0;
					m_player->getComponent<CState>()->state = "StandRight";
				}
			}
		}

		//Limit player speed
		m_player->getComponent<CTransform>()->speed.x = fmin(m_playerConfig.SPEED, fmax(m_player->getComponent<CTransform>()->speed.x, -m_playerConfig.SPEED));
		m_player->getComponent<CTransform>()->speed.y = fmin(m_playerConfig.SPEED, fmax(m_player->getComponent<CTransform>()->speed.y, -m_playerConfig.SPEED));
	}

	//All other entities
	for (auto entity : m_entityManager.getEntities())
	{
		if (entity->hasComponent<CTransform>())
		{
			entity->getComponent<CTransform>()->prevPos = entity->getComponent<CTransform>()->pos;
			entity->getComponent<CTransform>()->pos += entity->getComponent<CTransform>()->speed;

			//Update the sword's position as it follows with the player
			//Updates for the next frame
			if (entity->tag() == "sword")
			{
				m_player->getComponent<CInput>()->shoot = false;
				entity->getComponent<CTransform>()->facing = m_player->getComponent<CTransform>()->facing;
				entity->getComponent<CTransform>()->pos = m_player->getComponent<CTransform>()->pos + m_player->getComponent<CTransform>()->facing * 64;
				m_player->getComponent<CState>()->state = "Attack";
			}
		}
	}
	
	
}

const int PATROL_DISTANCE_MARGIN = 5;
void GameState_Play::sAI()
{
	for (auto entity : m_entityManager.getEntities("npc"))
	{
		Vec2 destination(0, 0);
		float speed;
		
		entity->getComponent<CTransform>()->speed = Vec2(0, 0);

		//Patrol NPCs
		if (entity->hasComponent<CPatrol>())
		{
			destination = entity->getComponent<CPatrol>()->positions[entity->getComponent<CPatrol>()->currentPosition];
			speed = entity->getComponent<CPatrol>()->speed;

			//If enemy is within a margin of PATROL DISTANCE, move to player
			if (entity->getComponent<CTransform>()->pos.dist(destination) <= PATROL_DISTANCE_MARGIN)
			{
				entity->getComponent<CPatrol>()->currentPosition += 1;

				if (entity->getComponent<CPatrol>()->positions.size() <= entity->getComponent<CPatrol>()->currentPosition)
				{
					entity->getComponent<CPatrol>()->currentPosition = 0;
				}

				destination = entity->getComponent<CPatrol>()->positions[entity->getComponent<CPatrol>()->currentPosition];
			}
		}
		//Follow NPCs
		else if (entity->hasComponent<CFollowPlayer>() && entity->hasComponent<CBoundingBox>())
		{
			bool inSight = true;
			speed = entity->getComponent<CFollowPlayer>()->speed;

			for (auto e : m_entityManager.getEntities())
			{
				//Skip player and itself
				if (e->tag() == "player" || entity == e) continue;

				if (e->hasComponent<CBoundingBox>() && e->getComponent<CBoundingBox>()->blockVision)
				{
					//If vision is blocked then cancel movement towards player
					if (Physics::EntityIntersect(m_player->getComponent<CTransform>()->pos, entity->getComponent<CTransform>()->pos, e) || entity->getComponent<CTransform>()->pos.dist(m_player->getComponent<CTransform>()->pos) > 10 * 64) // Check if sight is blocked by entity OR out of range
					{
						inSight = false;
						break;
					}
				}
			}

			//Move towards player if in sight
			if (inSight == true)
			{
				destination = m_player->getComponent<CTransform>()->pos;
			}
			else
			{
				destination = entity->getComponent<CFollowPlayer>()->home;
			}		
		}

		//Handle NPC speed
		if (entity->getComponent<CTransform>()->pos.dist(destination) <= PATROL_DISTANCE_MARGIN)
		{
			entity->getComponent<CTransform>()->speed = Vec2(0, 0);
		}
		else
		{
			float deltaX, deltaY, tanAngle;
			
			deltaX = destination.x - entity->getComponent<CTransform>()->pos.x;
			deltaY = destination.y - entity->getComponent<CTransform>()->pos.y;
			tanAngle = atan2(deltaY, deltaX);

			entity->getComponent<CTransform>()->speed = Vec2(speed * cos(tanAngle), speed * sin(tanAngle));
		}
	}
}

void GameState_Play::sLifespan()
{
	for (auto & entity : m_entityManager.getEntities()) {
		if (entity->hasComponent<CLifeSpan>() && !m_paused)
		{
			const float ratio = entity->getComponent<CLifeSpan>()->clock.getElapsedTime().asMilliseconds() / (float)entity->getComponent<CLifeSpan>()->lifespan;

			//Player falling
			if (entity == m_player)
			{
				if (ratio >= 1.0)
				{
					entity->destroy();
					spawnPlayer();
				}
				else
				{
					entity->getComponent<CTransform>()->scale *= 1.0 - ratio;
				}
			}
			//Fade in transition
			else if (entity == m_fadeIn)
			{
				if (ratio >= 1.0)
				{
					m_fadeIn->getComponent<CRectangleShape>()->shape.setFillColor(sf::Color(0, 0, 0, 0));
					m_fadeIn->removeComponent<CLifeSpan>();
				}
				else
				{
					m_fadeIn->getComponent<CRectangleShape>()->shape.setFillColor(sf::Color(0, 0, 0, 255 * (1.0 - ratio)));
					m_fadeIn->getComponent<CText>()->text.setFillColor(sf::Color(255, 0, 0, 255 * (1.0 - ratio)));
				}
			}
			else
			{
				if (ratio >= 1.0)
				{
					//Gravity well enable/disable toggle
					if (entity->tag() == "tile" && entity->hasComponent<CGravity>())
					{
						entity->getComponent<CGravity>()->enabled = !entity->getComponent<CGravity>()->enabled;
						entity->addComponent<CLifeSpan>(5000);

						//Determine animation based on state
						if (entity->getComponent<CGravity>()->enabled)
						{
							entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("GravityWell"), true);
						}
						else
						{
							entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("GravityWellDisabled"), true);
						}
					}
					else
					{
						entity->destroy();
					}
				}
			}
		}
	}
}


static const float PLAYER_LEDGE_MARGIN = 0.2f;
void GameState_Play::sCollision()
{
	//Margins to determine how far a player must be in the overlap to be considered 'collided'
	const float playerMarginX = m_player->getComponent<CBoundingBox>()->size.x * PLAYER_LEDGE_MARGIN;
	const float playerMarginY = m_player->getComponent<CBoundingBox>()->size.x * PLAYER_LEDGE_MARGIN;

	for (auto & tile : m_entityManager.getEntities("tile")) 
	{
		//Skip tiles that block movement
		if (!tile->getComponent<CBoundingBox>()->blockMove) { continue; }

		Vec2 overlap = Physics::GetOverlap(m_player, tile);
		Vec2 prevOverlap = Physics::GetPreviousOverlap(m_player, tile);

		const auto tileType = tile->getComponent<CAnimation>()->animation.getName();
		const auto tileTransform = tile->getComponent<CTransform>();
		const auto playerTransform = m_player->getComponent<CTransform>();
		auto playerState = m_player->getComponent<CState>();

		//Collision happens if theres an overlap in both axis
		if (overlap.x > 0 && overlap.y > 0) {
			
			//Player vs hole
			if (tileType.find("DunHole") != std::string::npos)
			{
				if (playerState->state != "Dead")
				{
					if (overlap.x >= playerMarginX && overlap.y >= playerMarginY)
					{
						playerState->state = "Dead";
						m_player->getComponent<CTransform>()->pos = tile->getComponent<CTransform>()->pos;
						m_player->addComponent<CLifeSpan>(2500);
					}
				}
				continue;
			}

			//Player vs fire
			if (tileType.find("Fire") != std::string::npos)
			{
				if (m_player->getComponent<Armor>()->armor > 0)
				{
					m_player->getComponent<Armor>()->armor--;
				}
				else
				{
					m_player->getComponent<Health>()->HP--;
					if (m_player->getComponent<Health>()->HP == 0)
					{
						m_player->destroy();
						spawnPlayer();
					}
				}
			}

			//Player vs tile top
			if (prevOverlap.x > 0 && tileTransform->prevPos.y < playerTransform->prevPos.y)
			{
				playerTransform->pos.y += overlap.y;
				playerTransform->speed.y = 0;

				if (playerTransform->speed.x == 0)
				{
					playerState->state = "standing";
				}
				else
				{
					playerState->state = "running";
				}
			}
			//Player vs tile bottom
			else if (prevOverlap.x > 0 && tileTransform->prevPos.y > playerTransform->prevPos.y)
			{
				playerTransform->pos.y -= overlap.y;
				playerTransform->speed.y = 0;
			}

			//Player vs tile left
			else if (prevOverlap.y > 0 && tileTransform->prevPos.x < playerTransform->prevPos.x)
			{
				playerTransform->pos.x += overlap.x;
			}

			//Player vs tile right
			else if (prevOverlap.y > 0 && tileTransform->prevPos.x > playerTransform->prevPos.x)
			{
				playerTransform->pos.x -= overlap.x;
			}
			else if (prevOverlap.y > 0 && m_game.window().getSize().x > playerTransform->prevPos.x)
			{
				playerTransform->pos.x -= overlap.x;
			}
		}

		//Special vs tile
		for (auto & special : m_entityManager.getEntities("special"))
		{
			Vec2 overlap = Physics::GetOverlap(special, tile);

			if (overlap.x > 0 && overlap.y > 0)
			{
				//Special dies if hits tile
				special->destroy();
			}
		}
	}

	//Player vs enemy NPCs
	for (auto npc : m_entityManager.getEntities("npc"))
	{
		Vec2 overlap = Physics::GetOverlap(m_player, npc);
		std::string npcType = npc->getComponent<CAnimation>()->animation.getName();

		if (overlap.x > 0 && overlap.y > 0)
		{
			//Neutral NPC
			if (npcType == "FlyingCarpet" && overlap.x >= playerMarginX && overlap.y >= playerMarginY)
			{
				m_player->getComponent<CTransform>()->pos += npc->getComponent<CTransform>()->speed;
			}
			else
			{
				//Hostile NPC
				if (m_player->getComponent<Armor>()->armor > 0)
				{
					m_player->getComponent<Armor>()->armor--;
				}
				else
				{
					m_player->getComponent<Health>()->HP--;
					if (m_player->getComponent<Health>()->HP == 0)
					{
						m_player->destroy();
						spawnPlayer();
					}
				}
			}
		}

		//NPC vs sword
		for (auto & sword : m_entityManager.getEntities("sword"))
		{
			Vec2 overlap = Physics::GetOverlap(sword, npc);

			if (overlap.x > 0 && overlap.y > 0)
			{
				// If NPC hit with sword, decrease HP
				npc->getComponent<Health>()->HP--;
				
				// If NPC HP is 0, kill it 
				if (npc->getComponent<Health>()->HP == 0 && npc->getComponent<CAnimation>()->animation.getName() != "Explosion" && npc != NULL)
				{
					auto boom = m_entityManager.addEntity("dec");

					boom->addComponent<CTransform>()->pos = npc->getComponent<CTransform>()->pos;
					boom->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);

					npc->destroy();
				}

				// NPC dies if hit with sword
				npc->getComponent<Health>()->HP--;
				if (npc->getComponent<Health>()->HP == 0)
				{
					npc->destroy();
				}
			}
		}
		for (auto & sword : m_entityManager.getEntities("arrow"))
		{
			Vec2 overlap = Physics::GetOverlap(sword, npc);

			if (overlap.x > 0 && overlap.y > 0)
			{
				if (npc->getComponent<CAnimation>()->animation.getName() != "Explosion" && npc != NULL)
				{
					auto boom = m_entityManager.addEntity("dec");

					boom->addComponent<CTransform>()->pos = npc->getComponent<CTransform>()->pos;
					boom->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);
				}

				// NPC dies if hit with sword
				npc->getComponent<Health>()->HP--;
				if (npc->getComponent<Health>()->HP == 0)
				{
					npc->destroy();
				}
			}
		}

		for (auto & special : m_entityManager.getEntities("special"))
		{
			Vec2 overlap = Physics::GetOverlap(special, npc);

			if (overlap.x > 0 && overlap.y > 0)
			{
				// If NPC hit with special, decrease HP
				npc->getComponent<Health>()->HP--;

				if (npc->getComponent<CAnimation>()->animation.getName() != "Explosion" && npc->getComponent<Health>()->HP == 0)
				{
					auto boom = m_entityManager.addEntity("dec");

					boom->addComponent<CTransform>()->pos = npc->getComponent<CTransform>()->pos;
					boom->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);

					npc->destroy();
					special->destroy();
				}
			}
		}

		//NPC vs tiles
		for (auto & tile : m_entityManager.getEntities("tile"))
		{
			//Skip tiles that block movement
			if (!tile->getComponent<CBoundingBox>()->blockMove) { continue; }

			const auto npcTransform = npc->getComponent<CTransform>();
			Vec2 overlap = Physics::GetOverlap(npc, tile);
			Vec2 prevOverlap = Physics::GetPreviousOverlap(npc, tile);
			const auto tileTransform = tile->getComponent<CTransform>();

			if (overlap.x > 0 & overlap.y > 0)
			{
				if (overlap.x > 0 && overlap.y > 0) {
					//NPC vs tile top
					if (prevOverlap.x > 0 && tileTransform->prevPos.y < npcTransform->prevPos.y)
					{
						npcTransform->pos.y += overlap.y;
						npcTransform->speed.y = 0;
					}
					//NPC vs tile bottom
					else if (prevOverlap.x > 0 && tileTransform->prevPos.y > npcTransform->prevPos.y)
					{
						npcTransform->pos.y -= overlap.y;
						npcTransform->speed.y = 0;
					}

					//Player vs tile left
					else if (prevOverlap.y > 0 && tileTransform->prevPos.x < npcTransform->prevPos.x)
					{
						npcTransform->pos.x += overlap.x;
					}

					//NPC vs tile right
					else if (prevOverlap.y > 0 && tileTransform->prevPos.x > npcTransform->prevPos.x)
					{
						npcTransform->pos.x -= overlap.x;
					}
				}
			}
		}
		Vec2 iOverlap;
		for (auto x : m_entityManager.getEntities("Item"))
		{
			if (x->hasComponent<CItem>())
			{
				if (x->getComponent<CItem>()->isWeapon == 1)
				{
					iOverlap = Physics::GetOverlap(m_player, x);
					if (iOverlap.x > 0 && iOverlap.y > 0)
					{
						// adds asset string to the weapons vector
						m_player->getComponent<CInventory>()->weapons.push_back(x->getComponent<CAnimation>()->animation.getName());
						// swtiches the current weapon to the one just picked up
						m_player->getComponent<CInventory>()->sel = m_player->getComponent<CInventory>()->weapons.size() - 1;
						x->destroy();


					}
				}
				if (x->getComponent<CItem>()->isWeapon == 0 && x->getComponent<CItem>()->hasLight == 0)
				{
					iOverlap = Physics::GetOverlap(m_player, x);
					if (iOverlap.x > 0 && iOverlap.y > 0)
					{
						m_player->getComponent<Health>()->HP += 50;
						x->destroy();

					}
				}
				if (x->getComponent<CItem>()->hasLight == 1 && x->getComponent<CItem>()->isWeapon == 0)
				{
					iOverlap = Physics::GetOverlap(m_player, x);
					if (iOverlap.x > 0 && iOverlap.y > 0)
					{
						light = 6;
						x->destroy();

					}
				}
			}
		}

		//Exit
		if (m_exit)
		{
			auto playerExitOverlay = Physics::GetOverlap(m_player, m_exit);
			if (playerExitOverlay.x > 0 && playerExitOverlay.y > 0)
			{
				m_levelCompleted = true;
			}
		}
	}
}

void GameState_Play::sGravity()
{
	for (auto & tile : m_entityManager.getEntities("tile"))
	{
		//Skip if tile doesn't have gravity
		if (!tile->hasComponent<CGravity>()) { continue; }

		//Skip if gravity is not enabled
		if (!tile->getComponent<CGravity>()->enabled) { continue; }

		const auto tileTransform = tile->getComponent<CTransform>();
		const auto playerTransform = m_player->getComponent<CTransform>();

		//Player gravity
		if (playerTransform->pos.dist(tileTransform->pos) <= tile->getComponent<CGravity>()->radius)
		{
			Vec2 direction = playerTransform->pos - tile->getComponent<CTransform>()->pos;
			float magnitude = sqrt(pow(direction.x, 2.0f) + pow(direction.y, 2.0f));
			Vec2 gravity = Vec2(tile->getComponent<CGravity>()->intensity * -direction.x / magnitude, tile->getComponent<CGravity>()->intensity * -direction.y / magnitude);

			playerTransform->pos += gravity;
		}

		for (auto & npc : m_entityManager.getEntities("npc"))
		{
			//FlyingCarpets are not affected
			if (npc->getComponent<CAnimation>()->animation.getName() == "FlyingCarpet") { continue; }

			const auto npcTransform = npc->getComponent<CTransform>();

			//NPC gravity
			if (npcTransform->pos.dist(tileTransform->pos) <= tile->getComponent<CGravity>()->radius)
			{
				Vec2 direction = npcTransform->pos - tile->getComponent<CTransform>()->pos;
				float magnitude = sqrt(pow(direction.x, 2.0f) + pow(direction.y, 2.0f));
				Vec2 gravity = Vec2(tile->getComponent<CGravity>()->intensity * -direction.x / magnitude, tile->getComponent<CGravity>()->intensity * -direction.y / magnitude);

				npcTransform->pos += gravity;
			}
		}
	}
}

void GameState_Play::sAnimation()
{
	for (auto entity : m_entityManager.getEntities()) 
	{
		//Skip entity if it doesn't have an animation
		if (!entity->hasComponent<CAnimation>()) { continue; }
		auto animation = entity->getComponent<CAnimation>();

		//Handle player animation
		if (entity == m_player)
		{
			if (m_player->getComponent<CState>()->state == "Dead")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("Dead"), true);
			}
			//Vertical running
			else if (m_player->getComponent<CState>()->state == "RunUp" && animation->animation.getName() != "RunUp")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("RunUp"), true);
			}
			else if (m_player->getComponent<CState>()->state == "RunDown" && animation->animation.getName() != "RunDown")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("RunDown"), true);
			}
			//Horizontal running
			else if (m_player->getComponent<CState>()->state == "RunLeft" || m_player->getComponent<CState>()->state == "RunRight")
			{
				if (animation->animation.getName() != "RunRight")
				{
					m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("RunRight"), true);
				}
				m_player->getComponent<CTransform>()->scale.x = m_player->getComponent<CTransform>()->facing.x;
			}
			//Vertical standing
			else if (m_player->getComponent<CState>()->state == "StandUp" && animation->animation.getName() != "StandUp")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandUp"), true);
			}
			else if (m_player->getComponent<CState>()->state == "StandDown" && animation->animation.getName() != "StandDown")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandDown"), true);
			}
			//Horizontal standing
			else if (m_player->getComponent<CState>()->state == "StandLeft" || m_player->getComponent<CState>()->state == "StandRight")
			{
				if (animation->animation.getName() != "StandRight")
				{
					m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandRight"), true);
				}
				m_player->getComponent<CTransform>()->scale.x = m_player->getComponent<CTransform>()->facing.x;
			}
		}

		//Attack down
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(0, 1))
		{
			auto attack_down = m_game.getAssets().getAnimation("AtkDown");
			m_player->getComponent<CAnimation>()->animation = attack_down;
		}

		//Attack up
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(0, -1))
		{
			auto attack_up = m_game.getAssets().getAnimation("AtkUp");
			m_player->getComponent<CAnimation>()->animation = attack_up;
		}

		//Attack right
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(-1, 0))
		{
			auto attack_right = m_game.getAssets().getAnimation("AtkRight");
			m_player->getComponent<CAnimation>()->animation = attack_right;
		}

		//Attack left
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(1, 0))
		{
			auto attack_right = m_game.getAssets().getAnimation("AtkRight");
			m_player->getComponent<CAnimation>()->animation = attack_right;
			
		}

		//Sword
		if (entity->tag() == "sword")
		{
			entity->getComponent<CTransform>()->scale = Vec2(1, 1);
			auto swordFace = entity->getComponent<CTransform>()->facing.y;

			//Handle facing
			if (abs(swordFace) == 1)
			{
				entity->addComponent<CAnimation>(m_game.getAssets().getAnimation(m_player->getComponent<CInventory>()->wepMapUp[m_player->getComponent<CInventory>()->weapons[m_player->getComponent<CInventory>()->sel]]), true);
				//entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordUp"), true);
				entity->getComponent<CTransform>()->scale.y = entity->getComponent<CTransform>()->facing.y * -1;
			}
			else
			{
				entity->addComponent<CAnimation>(m_game.getAssets().getAnimation(m_player->getComponent<CInventory>()->wepMapRight[m_player->getComponent<CInventory>()->weapons[m_player->getComponent<CInventory>()->sel]]), true);
				//entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordRight"), true);
				entity->getComponent<CTransform>()->scale.x = entity->getComponent<CTransform>()->facing.x;
			}
		}
		if (entity->tag() == "arrow")
		{
			Vec2 entityFace = m_player->getComponent<CTransform>()->facing;
			if (entityFace.x == 0)
			{
				entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("ArrowUp"), true);
			}
			else
			{
				entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("ArrowRight"), true);
			}

		}
		if (entity->tag() == "npc")
		{
			if (entity->getComponent<CAnimation>()->animation.getName() == "Monster4")
			{
				
			}
		}
		//Update
		animation->animation.update();

		//Destroy entity if animation does not repeat and has finish
		if (animation->animation.hasEnded() && !animation->repeat) {
			entity->destroy();
		}
	}
}

void GameState_Play::sUserInput()
{
    auto pInput = m_player->getComponent<CInput>();

    sf::Event event;
    while (m_game.window().pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_game.quit();
        }
        // this event is triggered when a key is pressed
        if (event.type == sf::Event::KeyPressed)
        {
			switch (event.key.code)
			{
			case sf::Keyboard::Escape: { m_game.popState(); break; }
			case sf::Keyboard::W: { pInput->up = true; break; }
			case sf::Keyboard::A: { pInput->left = true; break; }
			case sf::Keyboard::S: { pInput->down = true; break; }
			case sf::Keyboard::D: { pInput->right = true; break; }
			case sf::Keyboard::Z: { init(m_levelPath); break; }
			case sf::Keyboard::R: { m_drawTextures = !m_drawTextures; break; }
			case sf::Keyboard::F: { m_drawCollision = !m_drawCollision; break; }
			case sf::Keyboard::Y: { m_follow = !m_follow; break; }
			case sf::Keyboard::P: { setPaused(!m_paused); break; }
			case sf::Keyboard::Space: { spawnSword(m_player); break; }
			case sf::Keyboard::Enter: { spawnSpecial(m_player); break; }
			case sf::Keyboard::G: { m_displayShader = false; std::cout << "i hit g"; break; }
			case sf::Keyboard::N: { m_displayShader = true; std::cout << "i hit g"; break; }
			case sf::Keyboard::I: { m_viewChange = true; break; }
			case sf::Keyboard::Tab: {  m_game.pushState(std::make_shared<GameState_Inv>(m_game));  break; }
			case sf::Keyboard::Dash: { toggleWeaponLeft(); m_player->getComponent<CInventory>()->canToggle = false;  break; }
			case sf::Keyboard::Equal: { toggleWeaponRight(); m_player->getComponent<CInventory>()->canToggle = false; break; }
			
			}
        }

        if (event.type == sf::Event::KeyReleased)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::W:       { pInput->up = false; break; }
                case sf::Keyboard::A:       { pInput->left = false; break; }
                case sf::Keyboard::S:       { pInput->down = false; break; }
                case sf::Keyboard::D:       { pInput->right = false; break; }
                case sf::Keyboard::Space:   { pInput->shoot = false; pInput->canShoot = true; break; }
				case sf::Keyboard::Equal: { m_player->getComponent<CInventory>()->canToggle = true; break; }
				case sf::Keyboard::Dash: { m_player->getComponent<CInventory>()->canToggle = true; break; }
		
            }
        }

    }
}

static const int GUI_TEXT_SIZE = 20;
static const int GUI_TEXT_OFFSET_X = 10;
static const int GUI_TEXT_OFFSET_Y = 730;
static const int GUI_TEXT_SPACE = 200;
void GameState_Play::sRender()
{
    m_game.window().clear(sf::Color(105, 143, 160));
	sf::View playerView;

	//Window size
	const int windowX = m_game.window().getSize().x;
	const int windowY = m_game.window().getSize().y;
	playerView.setSize(windowX, windowY);

	//Player position
	float playerPosX = m_player->getComponent<CTransform>()->pos.x;
	float playerPosY = m_player->getComponent<CTransform>()->pos.y;

	//Room view offset
	const int roomOffsetX = floorf(m_player->getComponent<CTransform>()->pos.x / windowX) * windowX;
	const int roomOffsetY = floorf(m_player->getComponent<CTransform>()->pos.y / windowY) * windowY;

	//Set the camera to follow player's position 
	if (m_follow)
	{
		playerView.setCenter(sf::Vector2f(playerPosX, playerPosY));
	}
	//Set camera to overview the room 
	else if (!m_follow)
	{
		playerView.reset(sf::FloatRect(roomOffsetX, roomOffsetY, windowX, windowY));
	}
    
	m_game.window().setView(playerView);	
        
    //Draw all Entity textures / animations
    if (m_drawTextures)
    {
        for (auto e : m_entityManager.getEntities())
        {
			if (e == m_fadeIn) { continue; }

            auto transform = e->getComponent<CTransform>();

            if (e->hasComponent<CAnimation>())
            {
                auto animation = e->getComponent<CAnimation>()->animation;
                animation.getSprite().setRotation(transform->angle);
                animation.getSprite().setPosition(transform->pos.x, transform->pos.y);
                animation.getSprite().setScale(transform->scale.x, transform->scale.y);
                m_game.window().draw(animation.getSprite());
            }

			if (e->hasComponent<CRectangleShape>())
			{
				m_game.window().draw(e->getComponent<CRectangleShape>()->shape);
			}

			if (e->hasComponent<CText>())
			{
				m_game.window().draw(e->getComponent<CText>()->text);
			}
        }
    }

    //Draw all Entity collision bounding boxes with a rectangleshape
	if (m_drawCollision)
	{
		sf::CircleShape dot(4);
		dot.setFillColor(sf::Color::Black);
		for (auto e : m_entityManager.getEntities())
		{
			if (e->hasComponent<CBoundingBox>())
			{
				auto box = e->getComponent<CBoundingBox>();
				auto transform = e->getComponent<CTransform>();
				sf::RectangleShape rect;
				rect.setSize(sf::Vector2f(box->size.x - 1, box->size.y - 1));
				rect.setOrigin(sf::Vector2f(box->halfSize.x, box->halfSize.y));
				rect.setPosition(transform->pos.x, transform->pos.y);
				rect.setFillColor(sf::Color(0, 0, 0, 0));

				if (box->blockMove && box->blockVision) { rect.setOutlineColor(sf::Color::Black); }
				if (box->blockMove && !box->blockVision) { rect.setOutlineColor(sf::Color::Blue); }
				if (!box->blockMove && box->blockVision) { rect.setOutlineColor(sf::Color::Red); }
				if (!box->blockMove && !box->blockVision) { rect.setOutlineColor(sf::Color::White); }
				rect.setOutlineThickness(1);
				m_game.window().draw(rect);
			}

			if (e->hasComponent<CPatrol>())
			{
				auto & patrol = e->getComponent<CPatrol>()->positions;
				for (size_t p = 0; p < patrol.size(); p++)
				{
					dot.setPosition(patrol[p].x, patrol[p].y);
					m_game.window().draw(dot);
				}
			}

			if (e->hasComponent<CFollowPlayer>())
			{
				sf::VertexArray lines(sf::LinesStrip, 2);
				lines[0].position.x = e->getComponent<CTransform>()->pos.x;
				lines[0].position.y = e->getComponent<CTransform>()->pos.y;
				lines[0].color = sf::Color::Black;
				lines[1].position.x = m_player->getComponent<CTransform>()->pos.x;
				lines[1].position.y = m_player->getComponent<CTransform>()->pos.y;
				lines[1].color = sf::Color::Black;
				m_game.window().draw(lines);
				dot.setPosition(e->getComponent<CFollowPlayer>()->home.x, e->getComponent<CFollowPlayer>()->home.y);
				m_game.window().draw(dot);
			}
		}
	}

	/*
	Shader
	*/
	if (m_displayShader)
	{
		//Light circle
		sf::CircleShape circle;
		circle.setRadius(1500);
		circle.setOutlineColor(sf::Color::Red);
		circle.setFillColor(sf::Color::Transparent);
		circle.setOutlineThickness(5);
		circle.setPosition(m_player->getComponent<CTransform>()->pos.x, m_player->getComponent<CTransform>()->pos.y);
		circle.setOrigin(circle.getRadius(), circle.getRadius());

		//Darkness
		sf::RectangleShape rectangle;
		rectangle.setSize(sf::Vector2f(100, 200));
		rectangle.setOutlineColor(sf::Color::Black);
		rectangle.setFillColor(sf::Color::Black);
		rectangle.setOutlineThickness(5);
		rectangle.setPosition(100, 500);
		rectangle.setRotation(90);
		sf::Color black(0, 0, 0, 255);

		//Load vertex/fragment shaders
		sf::Shader shader;
		shader.loadFromMemory(SHADER_LIGHT_VERT, SHADER_LIGHT_FRAG);

		//Center circle over player
		int x = m_player->getComponent<CTransform>()->pos.x - roomOffsetX;
		int y = roomOffsetY + (windowY - m_player->getComponent<CTransform>()->pos.y);

		//Scale lighting with player's scale
		short radius = circle.getRadius() * abs(m_player->getComponent<CTransform>()->scale.x);

		if (m_follow)
		{
			x = m_game.window().getSize().x / 2.0f;
			y = m_game.window().getSize().y / 2.0f;
		}
			
		//Circle position
		sf::Vector3f lightCircleUniform = sf::Vector3f(x, y, radius);

		//Set uniforms
		shader.setParameter("light", lightCircleUniform);
		shader.setParameter("a", light);

		//Draw shader
		m_game.window().draw(circle, &shader);			
	}

		
    /*
	GUI
	*/

	//GUI offsets
	int healthOffsetX = roomOffsetX + GUI_TEXT_OFFSET_X;
	int armorOffsetX = roomOffsetX + GUI_TEXT_OFFSET_X + GUI_TEXT_SPACE;
	int invOffsetX = roomOffsetX + GUI_TEXT_OFFSET_X + 2*GUI_TEXT_SPACE;
	int offsetY = roomOffsetY + GUI_TEXT_OFFSET_Y;

	if (m_follow)
	{
		healthOffsetX = playerPosX - windowX / 2.0f + GUI_TEXT_OFFSET_X;
		armorOffsetX = playerPosX - windowX / 2.0f + GUI_TEXT_OFFSET_X + GUI_TEXT_SPACE;
		invOffsetX = playerPosX - windowX / 2.0f + GUI_TEXT_OFFSET_X + 2 * GUI_TEXT_SPACE;
		offsetY = playerPosY - windowY / 2.0f + GUI_TEXT_OFFSET_Y;
	}

	//Health
	sf::Text healthText;
	healthText.setFont(m_game.getAssets().getFont("Mana"));
	healthText.setCharacterSize(GUI_TEXT_SIZE);
	healthText.setFillColor(sf::Color::Red);
	healthText.setString("Health: " + std::to_string(m_player->getComponent<Health>()->HP));
	healthText.setPosition(sf::Vector2f(healthOffsetX, offsetY));

	//Armor
	sf::Text armorText;
	armorText.setFont(m_game.getAssets().getFont("Mana"));
	armorText.setCharacterSize(GUI_TEXT_SIZE);
	armorText.setFillColor(sf::Color::Red);
	armorText.setString("Armor: " + std::to_string(m_player->getComponent<Armor>()->armor));
	armorText.setPosition(sf::Vector2f(armorOffsetX, offsetY));

	//Draw GUI
	m_game.window().draw(healthText);
	m_game.window().draw(armorText);

	/*
	Transition
	*/
	m_game.window().draw(m_fadeIn->getComponent<CRectangleShape>()->shape);
	m_game.window().draw(m_fadeIn->getComponent<CText>()->text);
    
	//Display everything
    m_game.window().display();
}